import * as globalNS from 'src/namespace';

export interface IState {
    searchText: string;
    preloader: boolean;
    packagesList: globalNS.IPackageShortInfo[] | null,
    page: number;
    totalPages: number;

    packageDetails?: {
        name: string;
        filesIsLoading?: boolean;
        files?: globalNS.IPackageFile[];
    }
}
