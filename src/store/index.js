import Vue from 'vue';
import Vuex from 'vuex';
import { state } from './initialState';
import { actions } from './actions';
import { mutations } from './mutations';
import { getters } from './getters';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export const store = new Vuex.Store({
    state,
    actions,
    mutations,
    getters,
    strict: debug,
});
