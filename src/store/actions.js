import { loadPackages, loadPackageFiles } from 'src/api/api';

const PAGE_SIZE = 10;

export const actions = {
    async loadPackages({ commit }, { text, page }) {
        commit('LOAD_PACKAGES_REQUEST');
        try {
            const { packagesList, totalPages } = await loadPackages({ text, size: PAGE_SIZE, page });
            commit('CHANGE_SEARCH_TEXT', text);
            commit('LOAD_PACKAGES_SUCCESS', { packagesList, page, totalPages });
        } catch (error) {
            commit('LOAD_PACKAGES_ERROR', error);
        }
    },
    openPackageDetails({ commit, dispatch }, { name, version }) {
        commit('OPEN_PACKAGE_DETAILS', { name, version });
        dispatch('loadPackageFiles', { name, version });
    },
    closePackageDetails({ commit }) {
        commit('CLOSE_PACKAGE_DETAILS');
    },

    async loadPackageFiles({ commit }, { name, version }) {
        commit('LOAD_PACKAGE_FILES_REQUEST');
        try {
            const files = await loadPackageFiles({ name, version });
            commit('LOAD_PACKAGE_FILES_SUCCESS', files);
        } catch (error) {
            commit('LOAD_PACKAGE_FILES_FAIL', error);
        }
    },
};
