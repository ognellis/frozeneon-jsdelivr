import * as NS from './namespace';

export const state: NS.IState = {
    searchText: '',
    preloader: false,
    packagesList: null,
    page: 0,
    totalPages: 0,

    packageDetails: null,
};
