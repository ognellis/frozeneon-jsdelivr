import Vue from 'vue';
import * as globalNS from 'src/namespace';
import * as NS from './namespace';

export const mutations = {
    CHANGE_SEARCH_TEXT(state: NS.IState, searchText: string) {
        Vue.set(state, 'searchText', searchText);
    },
    LOAD_PACKAGES_REQUEST(state: NS.IState) {
        Vue.set(state, 'preloader', true);
    },
    LOAD_PACKAGES_SUCCESS(
        state: NS.IState,
        { packagesList, page, totalPages }: {packagesList:globalNS.IPackageShortInfo[], page: number, totalPages: number},
    ) {
        Vue.set(state, 'preloader', false);
        Vue.set(state, 'packagesList', packagesList);
        Vue.set(state, 'page', page);
        Vue.set(state, 'totalPages', totalPages);
    },
    LOAD_PACKAGES_ERROR(state: NS.IState) {
        Vue.set(state, 'preloader', false);
    },

    OPEN_PACKAGE_DETAILS(state: NS.IState, { name }: {name: string, version: string}) {
        Vue.set(state, 'packageDetails', { name });
    },
    CLOSE_PACKAGE_DETAILS(state: NS.IState) {
        Vue.set(state, 'packageDetails', null);
    },

    LOAD_PACKAGE_FILES_REQUEST(state: NS.IState) {
        Vue.set(state.packageDetails, 'filesIsLoading', true);
    },
    LOAD_PACKAGE_FILES_SUCCESS(state: NS.IState, files: globalNS.IPackageFile[]) {
        Vue.set(state.packageDetails, 'filesIsLoading', false);
        Vue.set(state.packageDetails, 'files', files);
    },
    LOAD_PACKAGE_FILES_FAIL(state: NS.IState) {
        Vue.set(state.packageDetails, 'filesIsLoading', false);
    },
};
