export const getters = {
    packagesList(state) {
        return state.packagesList;
    },
    page(state) {
        return state.page;
    },
    totalPages(state) {
        return state.totalPages;
    },
    preloader(state) {
        return state.preloader;
    },
    searchText(state) {
        return state.searchText;
    },
    packageDetails(state) {
        return state.packageDetails;
    },
    packageFilesPreloader(state) {
        return state.packageDetails && state.packageDetails.filesIsLoading;
    },
    packageFiles(state) {
        return state.packageDetails && state.packageDetails.files;
    },
};
