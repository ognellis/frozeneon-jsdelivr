import * as globalNS from 'src/namespace';
import * as NS from './namespace';

export function packageToClient(packageItem: NS.IPackageObjectServer): globalNS.IPackageShortInfo {
    return {
        name: packageItem.package.name,
        version: packageItem.package.version,
        description: packageItem.package.description,
        date: packageItem.package.date,
    };
}

export function packageFilesToClient(files: NS.IPackageFile[]): globalNS.IPackageFile[] {
    return files.map((file) => {
        if (file.type === 'file') {
            return {
                id: file.hash,
                name: file.name,
            };
        }
        if (file.type === 'directory') {
            return {
                id: file.name,
                name: file.name,
                children: packageFilesToClient(file.files),
            };
        }
        throw new Error();
    });
}
