export interface IUserServer {
    email: string;
    username: string;
}

export interface IPackageServer {
    name: string;
    scope: string;
    version: string;
    author: {
        name: string;
        url: string;
    };
    date: string;
    description: string;
    keywords: string[];
    links: {
        bugs: string;
        homepage: string;
        npm: string;
        repository: string;
    };
    maintainers: IUserServer[];
    publisher: IUserServer;
}

export interface IPackageObjectServer {
    package: IPackageServer;
    score: {
        detail: {
            maintenance: number;
            popularity: number;
            quality: number;
        };
        final: number;
    };
    searchScore: number;
}

export interface IPackagesResponse {
    objects: IPackageObjectServer[];
    time: string;
    total: number;
}

interface IFile {
    hash: string;
    name: string;
    size: number;
    time: string;
    type: 'file';
}

interface IDirectory {
    name: string;
    type: 'directory';
    files: Array<IDirectory | IFile>;
}

export type IPackageFile = IDirectory | IFile;

export interface IPackageFilesResponse {
    default: string;
    files: IPackageFile[];
}
