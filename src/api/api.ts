import axios from 'axios';
import * as converters from './converters';
import * as NS from './namespace';

export async function loadPackages({ text, size, page }: {text: string, size: number, page: number}) {
    try {
        const from = (page - 1) * size;
        const response = await axios.get<NS.IPackagesResponse>('http://registry.npmjs.com/-/v1/search', {
            params: {
                text,
                size,
                from,
            },
        });
        const packagesList = response.data.objects.map(converters.packageToClient);
        const totalPages = Math.ceil(response.data.total / size);
        return {
            packagesList,
            totalPages,
        };
    } catch (error) {
        throw error.data;
    }
}

export async function loadPackageFiles({ name, version }: {name: string, version: string}) {
    try {
        const response = await axios.get<NS.IPackageFilesResponse>(`https://data.jsdelivr.com/v1/package/npm/${name}@${version}/tree`);
        // console.log(response);
        const files = converters.packageFilesToClient(response.data.files);
        return files;
    } catch (error) {
        throw error.data;
    }
}
