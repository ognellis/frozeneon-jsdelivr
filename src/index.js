import Vue from 'vue';
import VueRouter from 'vue-router';
// import Vuetify from 'vuetify';
import app from './app.vue';
import { router } from './router';
import './style.scss';
import '@mdi/font/css/materialdesignicons.css';
import vuetify from './plugins/vuetify';
import { store } from './store';

Vue.use(VueRouter);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    vuetify,
    store,
    render: (h) => h(app),
});
