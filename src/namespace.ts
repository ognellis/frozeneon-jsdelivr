export interface IPackageShortInfo {
    name: string;
    version: string;
    description: string;
    date: string;
}

export interface IPackageFile {
    id: string;
    name: string;
    children?: IPackageFile[],
}
