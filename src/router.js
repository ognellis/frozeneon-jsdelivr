import VueRouter from 'vue-router';
import IndexPage from './pages/index.vue';

const routes = [
    { path: '/', component: IndexPage },
];

export const router = new VueRouter({
    mode: 'history',
    routes,
});
